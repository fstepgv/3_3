# 3_3

Запускаем командой ansible-playbook -i inventory playbook.yml

Вот такое получим на каждой машине:


root@ip-172-31-29-51:/etc/nginx/sites-available# ls


default  test1.juneway.pro  test2.juneway.pro


root@ip-172-31-29-51:/etc/nginx/sites-available# cat test1.juneway.pro



server {
    listen 80;
    listen [::]:80;
    server_name test1.juneway.pro;
    root /var/www/test1.juneway.pro;
    index index.html;
    try_files $uri /index.html;
}


root@ip-172-31-29-51:/etc/nginx/sites-available# cat test2.juneway.pro


server {
    listen 80;
    listen [::]:80;
    server_name test2.juneway.pro;
    root /var/www/test2.juneway.pro;
    index index.html;
    try_files $uri /index.html;
}